class Valuta{


    double value;
    
    public Valuta(double value){ 
        this.value = value;
    }

    public double getValue(){ //returnerer value til 
        return value;
    }

    public double convertTo(double value, double amount){ //converter
        return (this.value/value)*amount;
    }
    
}