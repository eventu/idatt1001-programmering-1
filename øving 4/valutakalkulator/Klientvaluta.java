import static javax.swing.JOptionPane.*;
class Klienvaluta{
    public static void main(String[] args){
   

        Valuta Nok = new Valuta(1);
        Valuta Sek = new Valuta(1.025);
        Valuta Usd = new Valuta(8.5);

        String[] fravaluta = new String[3];
        fravaluta[0] = "NOK";
        fravaluta[1] = "USD";
        fravaluta[2] = "SEK";
        Object valgtfraValuta = showInputDialog(null, "Velg valuta du vil konvertere fra:", "valutavalg", QUESTION_MESSAGE, null, fravaluta, "NOK");

        String[] tilvaluta = new String[3];
        tilvaluta[0] = "NOK";
        tilvaluta[1] = "USD";
        tilvaluta[2] = "SEK";
 
        Object valgttilValuta = showInputDialog(null, "Velg valuta du vil konvertere til:", "valutavalg", QUESTION_MESSAGE, null, tilvaluta, "USD");

        String beloplest = showInputDialog(null, "Velg beløp du ønsker å konvertere:"); // velger beløp som skal konverteres
        double belop = Double.parseDouble(beloplest); //tolker beløp

            if (valgtfraValuta == valgttilValuta) {
                showMessageDialog(null, "Vennligst velg to forskjellige valutaer" );
            }
            else if (valgtfraValuta == "NOK" && valgttilValuta == "SEK") {
                    showMessageDialog(null, "Det blir " + Nok.convertTo(Sek.getValue(), belop) +" svenske kroner.");
                }
            else if (valgtfraValuta == "NOK" && valgttilValuta == "USD") {
                    showMessageDialog(null, "Det blir " + Nok.convertTo(Usd.getValue(), belop)+ " amerikanske dollar.");
                }
            else if (valgtfraValuta == "USD" && valgttilValuta == "NOK") {
                    showMessageDialog(null, "Det blir " + Usd.convertTo(Nok.getValue(), belop) + " norske kroner.");
                }
            else if (valgtfraValuta == "USD" && valgttilValuta == "SEK") {
                    showMessageDialog(null, "Det blir " + Usd.convertTo(Sek.getValue(), belop) + " svenske kroner.");
                }
            else if (valgtfraValuta == "SEK" && valgttilValuta == "NOK") {
                    showMessageDialog(null, "Det blir " + Sek.convertTo(Nok.getValue(), belop) + " norske kroner.");
                }
            else if (valgtfraValuta == "SEK" && valgttilValuta == "USD") {
                    showMessageDialog(null, "Det blir " + Sek.convertTo(Usd.getValue(), belop) + " amerikanske dollar.");
                }
    }
}