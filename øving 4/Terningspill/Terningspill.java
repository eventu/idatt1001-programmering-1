// Øving 4 oppgave 2
//terningspillet 100

class Terningspill{
    java.util.Random terning = new java.util.Random();
    private int sumPoeng = 0;
    private int terningkast;
    public boolean erFerdig = false;

    //public Terningspill(){
    //}

    public int getSumPoeng(){
        return sumPoeng;
    }

    public void kastTerning(){
        terningkast = terning.nextInt(6) + 1; // +1 fordi terningen starter på 1 og ikke 0
        if (terningkast == 1){ //mister alle poeng ved kast 1
            sumPoeng = 0;
        }
          else { 
            sumPoeng += terningkast; //kastet legges til i sum
          }
    }

    public boolean erFerdig(){ //ferdig om man får over 100
        if (sumPoeng > 100) {
            erFerdig = true;
        } else {
            erFerdig = false;
        }
        return erFerdig;
    } 
}//class