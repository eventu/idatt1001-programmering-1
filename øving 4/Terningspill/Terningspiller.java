import java.util.Random;

class Terningspiller{
    public static void main(String[] args){
    int runde = 0;

    Terningspill spillera = new Terningspill();
    Terningspill spillerb = new Terningspill();

    while (!spillera.erFerdig() && !spillerb.erFerdig()) { // kaster terningen om ingen er ferdige (!)
        spillera.kastTerning();
        spillerb.kastTerning();
        runde ++ ;
        System.out.println("Poengsum spiller A runde " + runde + " : " + spillera.getSumPoeng());
        System.out.println("Poengsum spiller B runde " + runde + " : " + spillerb.getSumPoeng());
    }
    if (spillera.erFerdig()) { //avslutter om noen er ferdige
        System.out.println("Spiller A passerer 100 poeng og vinner! ");
    }
    else if (spillerb.erFerdig()) { 
        System.out.println("Spiller B passerer 100 poeng og vinner!");
    }
}//main
}//class