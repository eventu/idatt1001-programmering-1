import static javax.swing.JOptionPane.*;

//øving3 oppgave 1
//gangetabell fra 10-12

class Gangetabell {

public static void main(String[] args) { 
    String tablelest = showInputDialog("velg ønsket tabell"); // henter ønsket tall1
    int tablewanted = Integer.parseInt(tablelest); //tolker input
    String tablelest2 = showInputDialog("Velg tall nummer 2"); //henter tall 2
    int tablewanted2 = Integer.parseInt(tablelest2);
if (tablewanted > 9 && tablewanted < 13){
    showMessageDialog(null, tablewanted + "-tabellen:"  + "\n"+
         tablewanted + " x 1 = " + tablewanted * 1  + "\n"+
         tablewanted + " x 2 = " + tablewanted * 2  + "\n"+
         tablewanted + " x 3 = " + tablewanted * 3  + "\n"+
         tablewanted + " x 4 = " + tablewanted * 4  + "\n"+
         tablewanted + " x 5 = " + tablewanted * 5  + "\n"+
         tablewanted + " x 6 = " + tablewanted * 6  + "\n"+
         tablewanted + " x 7 = " + tablewanted * 7  + "\n"+
         tablewanted + " x 8 = " + tablewanted * 8  + "\n"+
         tablewanted + " x 9 = " + tablewanted * 9  + "\n"+
         tablewanted + " x 10 = " + tablewanted * 10  );
}
else {showMessageDialog(null, "velg et tall mellom 10 og 12"); }
} 
}