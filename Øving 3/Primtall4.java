import static javax.swing.JOptionPane.*;

class Primtall4 {
    public static void main(String args[]) {
        int choice;
        do {
            int temp; // for regnestykket
            boolean primTall = true;
            String lesttall = showInputDialog("Velg et tall:"); // henter et ønsket tall
            int tall = Integer.parseInt(lesttall); // tolker input
            if (tall <= 1) {
                showMessageDialog(null, tall + " Er ikke et primtall");
            } // 0 og 1 er ikke primtall
            for (int k = 2; k <= tall / 2; k++) { // bruker faktoren k
                temp = tall % k; // hvis temp blir 0 = tallet kan deles på en faktor k = ikke primtall
                if (temp == 0) {
                    primTall = false;
                }
            }
            if (primTall) {
                showMessageDialog(null, tall + " Er et primtall");
            } else {
                showMessageDialog(null, tall + " Er ikke et primtall!");
            }
            choice = showConfirmDialog(null, "Ønsker du å fortsette programmet?", "wtf", YES_NO_OPTION);
        } while (choice == YES_OPTION);
    } // main
} // class