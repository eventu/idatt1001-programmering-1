// øving 1 oppgave 2
// kalkulator fra minutter,time og sekunder til totalt antall sekunder
// tast inn antall timer

import static javax.swing.JOptionPane.*;

class Sekundkalkulator {
    public static void main(String[] args) {
        String timerlest = showInputDialog("Antall timer: "); // får inn antall timer
        String minutterlest = showInputDialog("Antall minutter: "); // får inn antall minutter
        String sekunderlest = showInputDialog("Antall sekunder: "); // får inn antall sekunder
        double timer = Double.parseDouble(timerlest); // lager timer
        double minutter = Double.parseDouble(minutterlest); // lager minutter
        double sekunder = Double.parseDouble(sekunderlest); // lager sekunder
        double sum = timer * 3600 + minutter * 60 + sekunder;
        showMessageDialog(null, "Det blir totalt " + sum + " sekunder");
    }
}
