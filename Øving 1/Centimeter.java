
// oppgave1 tommer til centimeter
// en tomme er 2.54 centimeter
import static javax.swing.JOptionPane.*;

class Centimeter {
    public static void main(String[] args) {
        String lengdeLest = showInputDialog("Lengde (tommer): ");
        double lengde = Double.parseDouble(lengdeLest);
        double konvertert = lengde * 2.54;
        showMessageDialog(null, "lengden blir " + konvertert + " centimeter");
    }
}
