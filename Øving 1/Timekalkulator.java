
// øving 1 oppgave 3 
// omregning fra sekunder til timer, minutter og sekunder
import static javax.swing.JOptionPane.*;

class Timekalkulator {
    public static void main(String[] args) {
        String sekunderlest = showInputDialog("Antall sekunder:");
        int sekunder = Integer.parseInt(sekunderlest); // integer bruker ikke desimaler
        int timersum = sekunder / 3600;
        int minuttersum = (sekunder - timersum * 3600) / 60; // trekker fra sekundene som ble brukt i timer
        int sekundersum = sekunder % 60; // % fordi det bruker kun rest
        showMessageDialog(null, "det blir totalt " + timersum + " timer og " + minuttersum + " minutter og "
                + sekundersum + " sekunder");
    }

}