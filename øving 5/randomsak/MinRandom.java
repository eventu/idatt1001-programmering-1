import java.util.Random;

class MinRandom {
    private Random random; //objektvariabelen

    public MinRandom() {
        random = new Random();//gir verdi til objektvariabelen
    }

    public int nesteHeltall(int nedre, int ovre) { //buker objektvariabelen
        return nedre + random.nextInt(ovre - nedre + 1);
    }

    public double nesteDesimaltall(double nedre, double ovre) {
        return nedre + (ovre - nedre) * random.nextDouble();
    }
}