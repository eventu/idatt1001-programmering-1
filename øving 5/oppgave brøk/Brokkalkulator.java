class Brokkalkulator{

    private int teller;
    private int nevner;

    public Brokkalkulator(int teller, int nevner){ //konstruktør
            this.teller = teller;
            this.nevner = nevner;
    }

    public Brokkalkulator(int teller){ //konstruktør
        this.teller = teller;
        nevner = 1;
    }

    public int getTeller(){
        return teller;
    }

    public int getNevner(){
        return nevner;
    }

    public void divisjon(int nyTeller, int nyNevner){
        teller *= nyNevner;
        nevner *= nyTeller;
    }

    public void addisjon(int nyTeller, int nyNevner){

        teller = (teller * nyNevner) + (nyTeller * nevner);
        nevner = (nevner * nyNevner);
    }

    public void multiplikasjon(int nyTeller, int nyNevner){
        teller *= nyTeller;
        nevner *= nyNevner;
    }
    
    public void subtraksjon(int nyTeller, int nyNevner){

        teller = (teller * nyNevner) - (nyTeller * nevner);
        nevner = (nevner * nyNevner);
    }
}
