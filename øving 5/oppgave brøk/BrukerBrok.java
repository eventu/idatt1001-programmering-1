import static javax.swing.JOptionPane.*;
class BrukerBrok{
    public static void main(String[] args){

        String teller = showInputDialog("Skriv inn teller 1: ");
        String nevner = showInputDialog("Skriv inn nevner 1: ");
        String nyTeller = showInputDialog("Skriv inn teller 2:");
        String nyNevner = showInputDialog("Skriv inn nevner 2:");
        int tellerLest = Integer.parseInt(teller);
        int nevnerLest = Integer.parseInt(nevner);
        int nyTellerLest = Integer.parseInt(nyTeller);
        int nyNevnerLest = Integer.parseInt(nyNevner);

        String[] operasjon = new String[4];
        operasjon[0] = "Addisjon";
        operasjon[1] = "Subraksjon";
        operasjon[2] = "Multiplikasjon";
        operasjon[3] = "Divisjon";

        Object valgtOperasjon = showInputDialog(null, "Velg hvilken operasjon du ønsker å utføre:", "operasjonvalg", QUESTION_MESSAGE, null, operasjon, "Addisjon");
        Brokkalkulator brok1 = new Brokkalkulator(tellerLest, nevnerLest);

            if (nevnerLest == 0){
            throw new IllegalArgumentException("Dele på null er tull, velg en annen nevner!");
            }
            else if (nyNevnerLest == 0){
                throw new IllegalArgumentException("Dele på null er tull, velg en annen nevner!");
            }
            else if (valgtOperasjon == operasjon[0]){
                brok1.addisjon(nyTellerLest,nyNevnerLest);
                showMessageDialog(null, "Den nye brøken har " + brok1.getTeller() + " som teller, og " + brok1.getNevner() + " som nevner.");
            }
            else if (valgtOperasjon == operasjon[1]){
                brok1.subtraksjon(nyTellerLest, nyNevnerLest);
                showMessageDialog(null, "Den nye brøken har " + brok1.getTeller() + " som teller, og " + brok1.getNevner() + " som nevner.");
            }
            else if (valgtOperasjon == operasjon[2]){
                brok1.multiplikasjon(nyTellerLest, nyNevnerLest);
                showMessageDialog(null, "Den nye brøken har " + brok1.getTeller() + " som teller, og " + brok1.getNevner() + " som nevner.");

            }
            else if ( valgtOperasjon == operasjon[3]){
                brok1.divisjon(nyTellerLest, nyNevnerLest);
                showMessageDialog(null, "Den nye brøken har " + brok1.getTeller() + " som teller, og " + brok1.getNevner() + " som nevner.");
            }
    }
}
