
//klientprogram øving8
import static javax.swing.JOptionPane.*;

import javax.swing.JOptionPane;

class Oving8 {
    public static void main(String[] args) {

        String navnLest = showInputDialog("Skriv inn navn: ");
        String fodselsAarLest = showInputDialog("Skriv inn fødselsår: ");
        String ansattNrLest = showInputDialog("Skriv inn ansattnummer: ");
        String startAarLest = showInputDialog("Skriv inn ansettelsesår: ");
        String maanedsLonnLest = showInputDialog("Skriv inn månedslønn: ");
        String skatteProsentLest = showInputDialog("Skriv inn skatteprosent: ");

        Person p1;
        ArbTaker arbTaker1;

        p1 = new Person(navnLest.split(" ")[0], navnLest.split(" ")[1], Integer.parseInt(fodselsAarLest));
        arbTaker1 = new ArbTaker(p1, Integer.parseInt(ansattNrLest), Integer.parseInt(startAarLest),
                Integer.parseInt(maanedsLonnLest), Double.parseDouble(skatteProsentLest));
                
        boolean runAgain = true; // kjører for første gang
        while (runAgain) {

            // p1= new Person("Petter", "Pellemann", 1999); //testansatt
            // arbTaker1 = new ArbTaker(p1, 1000, 2005, 40000, 25);

            String[] muligheter = new String[4];
            muligheter[0] = "Hente verdier";
            muligheter[1] = "Endre eksisterende verdier";
            muligheter[2] = "Se om den ansatte har vært ansatt i mer enn valg antall år";
            muligheter[3] = "Avslutte";

            Object valgtMulighet = showInputDialog(null, "hva vil du gjøre?", "Valg", QUESTION_MESSAGE, null,
                    muligheter, "Hente verdier");
            String printTekst = "";

            if (valgtMulighet == muligheter[0]) { //printer info om hente verdier
                printTekst = "Arbeidstakernummer: " + arbTaker1.getArbtakerNr();
                printTekst += "\n Ansettelsesår: " + arbTaker1.getAnsettelsesAar();
                printTekst += "\n Navn: " + arbTaker1.getEtterNavnforNavn();
                printTekst += "\n Alder: " + arbTaker1.hentAlder();
                //printTekst += "\n Etternavn: " + arbTaker1.personalia.getLastName();
                printTekst += "\n Fødselsår: " + arbTaker1.personalia.getBirthDay();
                printTekst += "\n Månedslønn: " + arbTaker1.getMaanedsLlonn();
                printTekst += "\n Skatteprosent: " + Double.toString(arbTaker1.getSkatteProsent()).replace(".", ",")
                        + "%";
                printTekst += "\n Skatt per måned: " + arbTaker1.skattPrMnd() + " Kroner";
                printTekst += "\n Brutto årslønn: " + arbTaker1.bruttoAarsLonn() + " Kroner";
                printTekst += "\n Skattetrekk per år: " + arbTaker1.skatteTrekkPerAar() + " Kroner";
                showMessageDialog(null, printTekst);
            } else if (valgtMulighet == muligheter[1]) { //ny vlagmulighet dersom valg endre verdier
                String[] endreVerdierMuligheter = { "Månedslønn", "Skatteprosent" };
                int valgSet = showOptionDialog(null, "hva vil du endre på?", "Endre verdier", 0, PLAIN_MESSAGE, null,
                        endreVerdierMuligheter, "Månedslønn");
                switch (valgSet) {
                    case 0:
                        String nyMndLonn = showInputDialog(null, "Skriv inn ny månedslønn:");
                        arbTaker1.setMaanedsLonn(Integer.parseInt(nyMndLonn));
                        break;
                    case 1:
                        String nySkatteProsent = showInputDialog(null, "Skriv inn ny skatteprosent:");
                        arbTaker1.setSkatteProsent(Double.parseDouble(nySkatteProsent.replace(",", ".")));
                        break;

                }
            } else if (valgtMulighet == muligheter[2]) { //skal se om ansattt lengre enn
                String lesAar = showInputDialog(null,
                        "Hvor mange år vil du se om den ansatte har vært ansatt lengre enn?");
                if (arbTaker1.ansattlengreEnn(Integer.parseInt(lesAar))) {
                    showMessageDialog(null, "Den ansatte har vært ansatt i mer enn " + lesAar + " år!");
                } else {
                    showMessageDialog(null, "Den ansatte har ikke vært ansatt i mer enn " + lesAar + " år!");
                }
            } else if (valgtMulighet == muligheter[3]) { //avslutt
                System.exit(0);
            }

            int reply = showConfirmDialog(null, "Vil du fortsette programmet?", null, YES_NO_OPTION); //fortsetter dersom ja, avslutter ellers
            if (reply == JOptionPane.YES_OPTION) {
                runAgain = true;
            } else {
                System.exit(0);
            }
        }
    }
}