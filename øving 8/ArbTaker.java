import java.util.*;
import java.util.GregorianCalendar.*;

class ArbTaker {
    public final Person personalia;
    public final int arbTakerNr;
    public final int ansettelsesAar;
    public int maanedsLonn;
    public double skatteProsent;
    java.util.GregorianCalendar kalender = new java.util.GregorianCalendar();


    public ArbTaker(Person personen, int arbTNr, int ansAar, int mndLonn, double skattepros) {
        this.personalia = personen;
        this.arbTakerNr = arbTNr;
        this.ansettelsesAar = ansAar;
        this.maanedsLonn = mndLonn;
        this.skatteProsent = skattepros;
    }

    public Person getPersonalia() {
        return personalia;
    }
    public int getArbtakerNr() {
        return arbTakerNr;
    }

    public int getAnsettelsesAar() {
        return ansettelsesAar;
    }

    public int getMaanedsLlonn() {
        return maanedsLonn;
    }

    public double getSkatteProsent() {
        return skatteProsent;
    }

    public void setMaanedsLonn(int nyMaanedsLonn) {
        maanedsLonn = nyMaanedsLonn;
    }

    public void setSkatteProsent(double nySkatteProsent) {
        skatteProsent = nySkatteProsent;
    }

    public double skattPrMnd() {
        double skattPrMnd = (skatteProsent / 100) * maanedsLonn;
        return skattPrMnd;
    }

    public int bruttoAarsLonn() {
        int bruttoLonn = maanedsLonn * 12;
        return bruttoLonn;
    }

    public double skatteTrekkPerAar() {
        double skattetrekkAar = maanedsLonn * (skatteProsent / 100) * 10.5;
        return skattetrekkAar;
    }

    public String getEtterNavnforNavn() {
        return personalia.getLastName() + ", " + personalia.getFirstName();
    }

    public int hentAlder() {
        return kalender.get(Calendar.YEAR) - personalia.getBirthDay();
    }

    public String hentFulltNavn(){
        return personalia.getFirstName() + "  " + personalia.getLastName();
    }

    public boolean ansattlengreEnn(int aar){
        if((kalender.get(Calendar.YEAR) - ansettelsesAar) > aar){
            return true;
        } else {
            return false;
        }
    }



}
