class Person{
    private final String firstName;
    private final String lastName;
    private final int birthDate;

    public Person(String fName, String lName, int bDate){
        this.firstName = fName;
        this.lastName = lName;
        this.birthDate = bDate;
    }

    public String getFirstName(){
        return firstName;
    }

    public String getLastName(){
        return lastName;
    }

    public int getBirthDay(){
        return birthDate;
    }
}