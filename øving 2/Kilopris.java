// øving 2 oppgave 2 kilopris

import static javax.swing.JOptionPane.*;
class Kilopris {
    public static void main(String[] args) {
        String prislesta = showInputDialog("Tast inn pris produkt A: "); // henter pris A
        String vektlesta = showInputDialog("Tast inn vekt på produkt A (NB! i gram): "); // henter vekt A i gram
        String prislestb = showInputDialog("Tast inn pris på produkt B: "); 
        String vektlestb = showInputDialog("Tast inn vekt på produkt B (NB! i gram): "); 
        double prisa = Double.parseDouble(prislesta); // tolker innhenting av data
        double prisb = Double.parseDouble(prislestb);
        double vekta = Double.parseDouble(vektlesta);
        double vektb = Double.parseDouble(vektlestb);
        double kiloprisa = prisa / vekta * 1000;  // deler pris på gram og granger med 1000 for kilopris
        double kiloprisb = prisb / vektb * 1000; 
        if (kiloprisa > kiloprisb) // dersom prisA > prisB kommer meldingen
        showMessageDialog(null, " pris per kilo produkt A blir " + kiloprisa + "\n"+
        " pris per kilo produkt B blir " + kiloprisb + "\n"+
        " produkt B er billigst per kilo");
        else if (kiloprisb > kiloprisa) // hvis prisB > prisA kommer meldingen
        showMessageDialog(null, " pris per kilo produkt A blir " + kiloprisa + "\n"+
        " pris per kilo produkt B blir " + kiloprisb + "\n"+
        " produkt A er billigst per kilo");
    }
}

