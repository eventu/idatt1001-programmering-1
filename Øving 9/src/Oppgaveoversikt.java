//Ant oppgaver
public class Oppgaveoversikt {
    private Student[] studenter; //tabell skal opprettes i konstruktøren
    private int antStud = 0; //økes med 1 for hver ny student

    public Oppgaveoversikt(Student[] studenter, int antStud){
        this.studenter = studenter;
        this.antStud = antStud;
    }

    public Student getStudentByNavn(String navn){
        for(int i = 0; i < antStud; i++){
            if(studenter[i]==null){
                break;
            }
            if (studenter[i].getNavn().equals(navn)){
                return studenter[i];
            }
        }
        return null;
    }

    public int studenterRegistrert(){
        return antStud;
    }

    public int antOppgStudent(Student stud){
        return stud.getAntOppg();
    }

    public String studentNavnVedId(int studId){
        return studenter[studId].getNavn();
    }

    public void regNyStudent(Student nyStudent){ //registrer ny student, øker ant studenter, ny tabell
        antStud++;
        int antStudenterfor = studenter.length + 1;
        Student[] nyTabell = new Student[antStudenterfor];
        nyTabell[studenter.length] = nyStudent;

        for(int i = 0; i < studenter.length; i++){
            nyTabell[i] = studenter[i];
        }
        studenter = new Student[antStudenterfor];
        for (int i = 0; i < antStudenterfor; i++){
            studenter[i] = nyTabell[i];
        }
    }

    public void godkjennOppg(String studentNavn, int antNyeGodkjente){
        for(int i =0; i < studenter.length; i++){
            if(studenter[i].getNavn().equals(studentNavn)){
                studenter[i].okAntOppg(antNyeGodkjente);
            }
        }
    }


}
