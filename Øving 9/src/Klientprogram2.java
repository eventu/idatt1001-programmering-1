import javax.swing.*;
import static javax.swing.JOptionPane.*;
public class Klientprogram2 {
    public static void main(String[] args) {
        Student[] studenter = {new Student("Petter Pellemann",10), new Student("Even Tuverud", 100),//Liste med premade studenter
                new Student("Donald Trump", 1), new Student("Ivanka Trump", 20), new Student("Josef Fritz", 1), new Student("Harald Hårføner", 9999)};
        Oppgaveoversikt oppgOv = new Oppgaveoversikt(studenter,6);

        boolean runAgain = true; //kjører loopen første gnag
        while (runAgain) {

            String[] muligheter = new String[5];
            muligheter[0] = "Hente data";
            muligheter[1] = "Registrere ny student";
            muligheter[2] = "Registrere godkjent oppgave for en student";
            muligheter[3] = "Vise antall registrerte studenter";
            muligheter[4] = "Avslutte";
            Object valgtMulighet = showInputDialog(null, "Hva vil du gjøre?", "Valg", QUESTION_MESSAGE, null, muligheter, muligheter[0]);
            String printtekst = "";
            String studNavn = "";

            if (valgtMulighet == muligheter[0]) { //hente data

                String[] henteData = new String[oppgOv.studenterRegistrert()];
                for (int i = 0; i < oppgOv.studenterRegistrert(); i++) {
                    henteData[i] = oppgOv.studentNavnVedId(i);
                }
                studNavn = (String) showInputDialog(null, "Hvem vil du hente data for?", "Hente data", QUESTION_MESSAGE, null, henteData, henteData[0]);
                printtekst += "\n" + studNavn + " har " + oppgOv.antOppgStudent(oppgOv.getStudentByNavn(studNavn)) + " godkjente oppgaver";
                printtekst += "";
                showMessageDialog(null, printtekst, "info", QUESTION_MESSAGE);

            } else if (valgtMulighet == muligheter[1]) { //registrere ny stud
                String studNavnLest = showInputDialog("Hva heter studenten?");
                String studOppgLest = showInputDialog("Hvor mange oppgaver har studenten?");
                int studOppg = Integer.parseInt(studOppgLest);
                oppgOv.regNyStudent(new Student(studNavnLest, studOppg));

            } else if (valgtMulighet == muligheter[2]) { //registrere godkjent oppgave
                String[] endreValg = new String[oppgOv.studenterRegistrert()];
                for (int i = 0; i < oppgOv.studenterRegistrert(); i++) {
                    endreValg[i] = oppgOv.studentNavnVedId(i);
                }

                studNavn = (String) showInputDialog(null, "Hvem vil du endre for?", "Øke antall godkjente", QUESTION_MESSAGE, null, endreValg, endreValg[0]);
                String antallOppgLest = showInputDialog("Hvor mange vil du godkjenne?");
                int nyeGodkjenteOppgaver = Integer.parseInt(antallOppgLest);
                oppgOv.godkjennOppg(studNavn, nyeGodkjenteOppgaver);
            }

            else if (valgtMulighet == muligheter[3]){
                showMessageDialog(null, "Antall registrerte studenter: " + oppgOv.studenterRegistrert());
            }

            else if (valgtMulighet == muligheter[4]) { //avslutte
                System.exit(0);
            }

            int reply = showConfirmDialog(null, "Hvil du fortsette programmet?", null, YES_NO_OPTION);//fortsetter dersom ja, ellers avslutt
            if (reply == YES_OPTION) {
                runAgain = true;
            } else {
                System.exit(0);
            }
        }
    }
}
