import javax.swing.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.*;

public class PropertyRegister {
    public static ArrayList<Property> properties;

    /**
     * Ny liste med eiendommer
     */
    public PropertyRegister() {
        properties = new ArrayList<>();
    }

    /**
     *
     * @param municipalityName dssd
     * @param municipalitynr
     * @param lotNr
     * @param sectionNr
     * @param name
     * @param area
     * @param owner
     * @return
     */
    //Legger  til eiendom i listen properties
    public Property registerProperty(String municipalityName, int municipalitynr, int lotNr, int sectionNr, String name, double area, String owner) {
        Property newProperty = new Property(municipalityName, municipalitynr, lotNr, sectionNr, name, area, owner);
        properties.add(newProperty);
        return newProperty;
    }

    //metode som viser alle eiendommer, sortert på kommunenr
    public List<Property> showProperties() {
        return properties.stream()
                .sorted(Comparator.comparing(Property::getMunicipalitynr))
                .collect(Collectors.toList());
    }

    //metode som returnerer størrelsen på listen (antall eiendommer registrert)
    public int numProperties() {
        return properties.size();
    }

    // Iteratorgreier brukt til å fjerne fra listen (sjekker alle tre kriterier opp mot registeret)
    public static Property listWNum(int municipalityNr, int lotNr, int sectionNr) {
        Iterator<Property> iterator = properties.iterator();
        while (iterator.hasNext()) { //ikke lov å kalle next() uten først å ha sjekket at det finnes flere elementer (HasNext())
            Property nProperty = iterator.next();
            if (nProperty.getMunicipalitynr() == municipalityNr && nProperty.getLotNr() == lotNr && nProperty.getSectionNr()
                    == sectionNr) {
                return nProperty;
            }
        }
        return null;
    }


    /**
     *metode for å slette fra listen:
     * @param municipalityNr må stemme
     * @param lotNr må stemme
     * @param sectionNr må stemme
     * @return returnerer at den ble slettet korrekt dersom funnet, eller not found dersom en av kriteriene ikke ble funnet
     */
    public static String deleteProperty(int municipalityNr, int lotNr, int sectionNr){
        Property delete = listWNum(municipalityNr, lotNr, sectionNr);
        properties.remove(delete);
         if (delete != null) {
             return  delete + " \nProperty was deleted successfully";
        }
            return "Property not found, please try again";
    }


    //metode for å returndere alle eiendommer med et visst lotnr (oppgave 3C)
    public List<Property> findByLotNr(int lotnr)  {
        return properties.stream()
                .filter(p -> p.getLotNr() == lotnr)
                .collect(Collectors.toList());
    }


    /**
     * Metode for gjennomsnitt av areal (oppgave 3b), bruker også iterator
     * @return
     */
    public double avgArea(){
        Iterator<Property> iterator = properties.iterator();
        double totalArea = 0;
        while (iterator.hasNext()){//ikke lov å bruk .next før man har sjekket at hasnext = true!!!!
            Property s = iterator.next();
            totalArea += s.getArea();
        }
        return totalArea/ numProperties();
    }

    //Search specific property by muni,lot and section numbers
    public List<Property> checkProperty(int municipalityNr, int lotNr, int sectionNr){
        return properties.stream()
                .filter(p -> municipalityNr == p.getMunicipalitynr() && lotNr == p.getLotNr() && sectionNr == p.getSectionNr())
                .collect(Collectors.toList());
    }
}



//oppgave 3a jeg brukte arraylist fordi det er det jeg kan
//og ArrayList har flere ferdiglagde funksjoner som fekst stream, collect, filter osv
// Array er fixed size og jobb å hente forrige liste for å ta +1 +6 +10 feks
//ArrayList kan ha flere størrelser og øker / krymper automatisk
//ArrayList lager en array/liste med objecter, mens List er et interface som brukes/ blir implementert av arraylist