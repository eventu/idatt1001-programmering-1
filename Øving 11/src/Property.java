public class Property {
    private final String municipalityName;
    private final int municipalitynr;
    private final int lotNr;
    private final int sectionNr;
    private final String name;
    private final double area;
    private final String owner;

    /**
     *
     * @param municipalityName kommunenavn
     * @param municipalitynr kommunenummber
     * @param lotNr  Gårdsnummber
     * @param sectionNr  bruksnummer
     * @param name  navn på tomta
     * @param area  areal på tomta
     * @param owner navnet på eieren
     */
    public Property(String municipalityName, int municipalitynr, int lotNr, int sectionNr, String name, double area, String owner){
        this.municipalityName = municipalityName;
        this.municipalitynr = municipalitynr;
        this.lotNr = lotNr;
        this.sectionNr = sectionNr;
        this.name = name;
        this.area = area;
        this.owner = owner;
    }

    /**
     *
     * @return returnerer kommunenavn
     */
    public String getMunicipalityName(){

        return municipalityName;
    }

    /**
     *
     * @return returnerer kommunenavn
     */
    public int getMunicipalitynr(){
        return municipalitynr;
    }

    /**
     *
     * @return eturnerer gårdsnummer
     */
    public int getLotNr(){
        return lotNr;
    }

    /**
     *
     * @return returnerer bruksnunmmer
     */
    public int getSectionNr() {
        return sectionNr;
    }

    /**
     *
     * @return returnerer navnet på tomta
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @return  returnerer areal på tomta
     */
    public double getArea() {
        return area;
    }

    /**
     *
     * @return returnerer navnet på eieren
     */
    public String getOwner() {
        return owner;
    }

    /**
     *
     * @return toString som returnerer all info om tomten, mye brukt i visning av listene i klientprogrammet
     */
    public String toString() {
        return "\n" + "Municipality: " + getMunicipalityName()  + getMunicipalitynr()+"-"+getLotNr()+"/"+getSectionNr() + ", Name: " + getName() +", Area: " + getArea() + "m2, Owner: " + getOwner();
    }

//dette er oppgave 2b
    /**
     *
     * @return returnerer kun kommune-eiendom/bruksnummer på gyldig format om det skulle være behov for det
     */
    public String toSmallString(){ //toString som kun viser kommune/iendom/bruksnummer på rett format.
        return "Property: " + getMunicipalitynr()+"-"+getLotNr()+"/"+getSectionNr();
    }








}//main

