import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Iterator.*;
import java.util.stream.Collectors;
import static javax.swing.JOptionPane.*;

public class Client {
    public static void main(String[] args) {
        PropertyRegister propertyRegister = new PropertyRegister();
        propertyRegister.registerProperty("Gloppen", 1445, 77, 631,"", 1017.6,"jens Olsen");
        propertyRegister.registerProperty("Gloppen", 1445, 77, 131,"Syningom", 661.3,"Nicolay Madsen");
        propertyRegister.registerProperty("Gloppen", 1445, 75, 19,"Fugletun", 650.6,"Evilyn Jensen");
        propertyRegister.registerProperty("Gloppen", 1445, 74, 188,"", 1457.2,"Karl Ove Bråten");
        propertyRegister.registerProperty("Gloppen", 1445, 69, 47,"Høiberg", 1339.4,"Elsa Indregård");

        /**
         *  Loop for at programmet skal fortsette å gå etter en handling
         */

        
        boolean runAgain = true;
        while (runAgain) {


            /**
             * Her er menyen
             */
            String[] options = new String[8];
            options[0] = "Register new property";
            options[1] = "Show all properties";
            options[2] = "Delete a property from list";
            options[3] = "Show number of properties";
            options[4] = "Search for property by lotNr";
            options[5] = "Calculate average area of properties";
            options[6] = "Search for property by municipality-lot/section numbers";
            options[7] = "Exit program";
            Object selectedOption =showInputDialog(null, "What do you want to do?", "Options", QUESTION_MESSAGE,null, options, options[0]);


            while (selectedOption == options[0]) { //Register new prop  WHILE for å kunne breake om ugyldig nummer
                String regMunicipalityName = showInputDialog(null, "Write name of the municipality: ");
                String municipalityNrRead = showInputDialog(null, "Write municipalitynumer: \n Must be between 101 and 5054!");
                int regMunicipalityNr = Integer.parseInt(municipalityNrRead);
                if(regMunicipalityNr <101 || regMunicipalityNr > 5054){
                    showMessageDialog(null, "Illegal input! Try again!");// dersom nr er under 101/over 5054 > feilmeld og breaker
                    break;
                }
                String lotNrRead = showInputDialog(null, "Write lotnr:");
                int regLotNr = Integer.parseInt(lotNrRead);
                if (regLotNr <= 0){
                    showMessageDialog(null, "must be above 0!");
                    break;
                }
                String sectionNrRead = showInputDialog(null, "Write sectionNumber:");
                int regSectionNr = Integer.parseInt(sectionNrRead);
                if (regSectionNr  <= 0){
                    showMessageDialog(null, "must be above 0!");
                    break;
                }
                String regPropertyName = showInputDialog("Write name of the property: \n (Leave blank if none)");
                String areaRead = showInputDialog("Write the area of the property:");
                int regArea = Integer.parseInt(areaRead);
                if (regArea <= 0){
                    showMessageDialog(null, "Must be above 0!");
                    break;
                }
                String regOwner = showInputDialog("What is the name of the owner:");
                propertyRegister.registerProperty(regMunicipalityName, regMunicipalityNr, regLotNr, regSectionNr, regPropertyName, regArea, regOwner);
                break;
            }

            if (selectedOption == options[1]) { //Vise alle eiendommer
                List<Property> showAll = propertyRegister.showProperties();
                showMessageDialog(null, "List of all properties: " + showAll);
            }

            if (selectedOption == options[2]) {//Slette:
                String deleteThisMunicipalityRead = showInputDialog(null, "Write municipalitynr of the property to be deleted:" + propertyRegister.showProperties());
                int deletehismunicipalityNr = Integer.parseInt(deleteThisMunicipalityRead);
                String deleteThisLotNrRead = showInputDialog(null, "... and the lotNr of the property to be deleted:" + propertyRegister.showProperties());
                int deleteThisLotNr = Integer.parseInt(deleteThisLotNrRead);
                String deleteThisRead = showInputDialog("... and write sectionumber of the property to be deleted:" + propertyRegister.showProperties());
                int deleteThisSectionNr = Integer.parseInt(deleteThisRead);
                showMessageDialog(null, PropertyRegister.deleteProperty(deletehismunicipalityNr,deleteThisLotNr,deleteThisSectionNr));
            }

            if (selectedOption == options[3]){ // Viser antall eiendommer
                showMessageDialog(null, "number of properties: " + propertyRegister.numProperties());
            }

            if (selectedOption == options[4]){ //Search by lotNr
                String chosenLotNrRead = showInputDialog(null, "Type lotnumber you want to find");
                int chosenLotNr = Integer.parseInt(chosenLotNrRead);
                List<Property> showChosenLotNr =propertyRegister.findByLotNr(chosenLotNr);
                if(showChosenLotNr.size()== 0){
                    showMessageDialog(null, "Could not find any properties with lotnumber " + chosenLotNr + ".");
                } else{
                    showMessageDialog(null, "Properties found with lotnr " + chosenLotNr + ": " + showChosenLotNr);
                }
            }

            if (selectedOption == options[5]){ //Calculate average area
                showMessageDialog(null, "the average area of all properties are " + propertyRegister.avgArea()+ "m2.");
            }

            if (selectedOption == options[6]) { //search by municipality-Lot/section number(Alle tre)
                String municipalityNrRead = showInputDialog(null,"What municipalitynumber?");
                String lotNrRead = showInputDialog(null, "What lot-number?");
                String sectionNrRead = showInputDialog(null, "What section-number?");
                int municipalityNr = Integer.parseInt(municipalityNrRead);
                int lotNr = Integer.parseInt(lotNrRead);
                int sectionNr = Integer.parseInt(sectionNrRead);
                List<Property> searchResults = propertyRegister.checkProperty(municipalityNr,lotNr,sectionNr);
                if(searchResults.size() == 0){
                    showMessageDialog(null, "No results, please try again.");//Dersom det ikke svarer med noen i listen
                } else{
                    showMessageDialog(null, "Results: " + searchResults);//resultater
                }
            }

            if (selectedOption == options[7]){//Avslutte programmet
                System.exit(0);
            }
            int reply =showConfirmDialog(null,"Continue?:", "Continue?", YES_NO_OPTION);
            if (reply == YES_OPTION) runAgain = true;
            else runAgain = false;
        } //Runagain
    }//main
}//Class